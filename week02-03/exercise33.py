import pandas as pd
import numpy as np

movies_df = pd.read_table('inputs/ml-1m/movies.dat', names=['movie ID', 'Title', 'genders'], sep='::')
# Transforming the genders column to a proper list
movies_df['genders'] = movies_df['genders'].apply(lambda x: x.split('|'))

users_df = pd.read_table('inputs/ml-1m/users.dat', names=['user ID', 'gender', 'age', 'occupation code', 'zip'],
                         sep='::')

ratings_df = pd.read_table('inputs/ml-1m/ratings.dat', names=['user ID', 'movie ID', 'rating', 'timestamp'], sep='::',
                           parse_dates=[3])
movie_data = pd.merge(ratings_df, users_df, left_on='user ID', right_on='user ID') \
    .merge(movies_df, left_on='movie ID', right_on='movie ID')

# The data frame is actually a table of ratings with all the available movie & submitting user information
active_titles = movie_data.groupby('movie ID').filter(lambda x: len(x['rating']) >= 250)

# Best mean ratings of active titles for male, sorted in decreasing order
active_best_male_ratings = active_titles[active_titles['gender'] == 'M'].groupby('movie ID').agg(
    {'rating': np.mean}).sort('rating', ascending=False)

# Best mean ratings of active titles for female, sorted in decreasing order
active_best_female_ratings = active_titles[active_titles['gender'] == 'F'].groupby('movie ID').agg(
    {'rating': np.mean}).sort('rating', ascending=False)

# Combined best mean ratings for both male and female
active_best_ratings = pd.concat([active_best_male_ratings, active_best_female_ratings], axis=1, keys=['male', 'female'])

# Adding a column for the difference between mean male and female ratings
active_best_ratings['diff'] = active_best_ratings[('male', 'rating')] - active_best_ratings[('female', 'rating')]

# Movie IDs for the titles with the biggest number of ratings
top5_ratings_count = movie_data.groupby('movie ID').agg({'rating': np.size}).sort('rating', ascending=False).head(5)

# The top3 movies in term of male ratings
top3_male = active_best_male_ratings.head(3)
# The top3 movies in term of female ratings
top3_female = active_best_female_ratings.head(3)

# The top10 movies that were liked more by male than female
top10_MaleOverFemale = active_best_ratings.sort('diff', ascending=False).head(10)
# The top10 movies that were liked more by female than male
top10_FemaleOverMale = active_best_ratings.sort('diff').head(10)

# The top5 of movies with biggest Standard Deviation in ratings
top5_std = active_titles.groupby('movie ID').agg({'rating': np.std}).sort('rating', ascending=False).head(5)
