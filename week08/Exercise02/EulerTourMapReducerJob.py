from mrjob.job import MRJob
from mrjob.step import MRStep


class EulerTourMapReduceJob(MRJob):

    def mapper_get_directed_edges(self, key, value):
        edge = value.split()
        yield edge[0], 1
        yield edge[1], 1

    def reducer_node_degree(self, key, values):
        yield key, sum(values)

    def mapper_get_graph(self, key, value):
        yield None, value

    def reducer_hasEulerTour(self, key, values):
        has_euler = True
        for degree in values:
            if degree % 2 == 1:
                has_euler = False
                break
        yield 'Has Euler Tour', has_euler

    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_directed_edges,
                   reducer=self.reducer_node_degree),
            MRStep(mapper=self.mapper_get_graph,
                   reducer=self.reducer_hasEulerTour)
        ]


if __name__ == '__main__':
    EulerTourMapReduceJob.run()
