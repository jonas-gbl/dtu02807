import numpy as np

Ab = np.genfromtxt("inputs/mx_input.txt", delimiter=",")
A = Ab[:, :-1]
b = Ab[:, -1]
x = np.linalg.solve(A, b)
print x
