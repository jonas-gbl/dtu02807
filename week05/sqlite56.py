import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT ProductID, COUNT(DISTINCT [orders].OrderID) AS count FROM"
                   "[order details] INNER JOIN [orders] USING (OrderID) "
                   "WHERE CustomerID IN (SELECT  [orders].CustomerID FROM [order details] "
                   "INNER JOIN orders ON [order details].OrderID = [orders].OrderID "
                   "WHERE   ProductID=7) AND ProductId != 7 GROUP BY ProductID ORDER BY count DESC LIMIT 1;")

    row = cursor.fetchone()

    print "The requested ProductID is %s with a total number of %s orders" % (row['ProductID'], row['count'])
