def read_matrix_file(input_filename):
    with open(input_filename, 'r') as inputFile:
        matrix = [[int(x) for x in line.split()] for line in inputFile]
    return matrix


def write_matrix_file(matrix, output_filename='outputs/output.txt'):
    with open(output_filename, 'w') as outputFile:
        for line in matrix:
            outputFile.write(" ".join(map(str, line)) + "\n")


def generate_binary_list(N):
    binary_matrix = []
    for dec in range(0, 2 ** N):
        bin_list = []
        for n in range(N - 1, -1, -1):
            bin_list.append(dec // 2 ** n)
            dec %= 2 ** n

        binary_matrix.append(bin_list)

    return binary_matrix


print generate_binary_list(3)
matrix = read_matrix_file('inputs/input.txt')
write_matrix_file(matrix)
