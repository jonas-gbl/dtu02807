from sklearn.ensemble import RandomForestClassifier
from glob import glob
import json
import math
import numpy as np
import jsonpath_rw as jpath

NUMBER_OF_PERMUTATIONS = 10

json_content = []
for json_filename in glob('./json/*.json'):
    with open(json_filename) as json_file:
        file_content = json.load(json_file)
        filtered_content = filter(lambda u: u'body' in u and len(u[u'body']) and u'topics' in u and len(u[u'topics']),
                                  file_content)
        json_content.extend(filtered_content)

word_idx = {}
global_counter = int(0)

for document in json_content:
    for word in document[u'body'].split():
        token = word.lower()
        # If the word has not been found before
        if token not in word_idx:
            # Assign a new index-number to it
            word_idx[token] = global_counter
            global_counter += 1

word_bags = np.zeros((len(json_content), len(word_idx)), dtype=np.int)

# Iterating through all the words in the request texts
for k in range(len(json_content)):
    for word in json_content[k][u'body'].split():
        token = word.lower()
        # Retrieve the index-number of the current word which was assigned previously
        index = word_idx[token]
        # Increment the counter of specific word on the current request-text
        word_bags[k, index] += 1

word_bags = word_bags.transpose()
perm = np.random.permutation(word_bags.shape[0])

n_columns = word_bags.shape[1]
buckets = {}
permutations = [np.random.permutation(word_bags.shape[0]) for _ in range(NUMBER_OF_PERMUTATIONS)]

for columnIndex in range(n_columns):
    bucket_key = ()
    for permutation in permutations:
        permutedColumn = word_bags[permutation, columnIndex]
        firstNonZero = permutedColumn.nonzero()[0][0]
        bucket_key += (firstNonZero,)
    if bucket_key in buckets:
        buckets[bucket_key].append(columnIndex)
    else:
        buckets[bucket_key] = [columnIndex]

sorted_buckets = sorted(buckets.values(), key=(lambda x: len(x)), reverse=True)
