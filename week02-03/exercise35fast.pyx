def fast_sum():
    cdef double result = 0.0
    cdef int k

    for k in range(1, 10000 + 1):
        result += 1.0 / k ** 2
    return result

