from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
northwind = client['Northwind']

orders = northwind['orders']
orderDetails = northwind['order-details']

pipeline = [{"$match": {"ProductID": 7}},
            {"$group": {"_id": "$ProductID", "Orders": {"$addToSet": "$OrderID"}, "Count": {"$sum": 1}}}]

ordersOfSeven = orderDetails.aggregate(pipeline).next()[u'Orders']
buyersOfSeven = [order[u'CustomerID'] for order in orders.find({"OrderID": {"$in": ordersOfSeven}})]

print "Total customers ordering ProductID=7: %s" % len(buyersOfSeven)
print ""
print "Customer ID"
for buyer in buyersOfSeven:
    print buyer
