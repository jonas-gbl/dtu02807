# Exercise 1
cat input.log | sed -r -e 's#\s+#\n#g' | sort | uniq -c | sort -r | head -n 10 | awk '{ print $2 "\t->\t" $1 }'
# Exercise 2
cat cars.txt | awk '{if($5<=10000){print $0}}'
# Exercise 3
# replace punctation with whitespace | convert everything to lowercase | remove leading whitespaces - remove empty lines - remove trailing whitespaces | truncate multiple whitespaces 
# | convert whitespace to newlines | sort lines/words | remove duplicates
cat shakespeare.txt | tr '[:punct:]' ' ' | tr '[:upper:]' '[:lower:]'| sed -r -e 's#^\s+##' -e '/^\s*$/d' -e 's#\s+$##' -e 's#\s+# #g' | tr ' ' '\n' | sort | uniq > shakespeare.clean.txt
# replace punctation with whitespace | convert everything to lowercase | remove leading whitespaces - remove empty lines - remove trailing whitespaces | truncate multiple whitespaces 
# | convert whitespace to newlines | sort lines/words | remove duplicates
cat dict | tr '[:punct:]' ' ' | tr '[:upper:]' '[:lower:]'| sed -r -e 's#^\s+##' -e '/^\s*$/d' -e 's#\s+$##' -e 's#\s+# #g' | tr ' ' '\n' | sort | uniq > dict.clean.txt
# extarct lines/words not i dictionary | count lines/words
comm -13 dict.clean.txt shakespeare.clean.txt | wc -l
