from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
northwind = client['Northwind']

orderIDs = [10248, 10296]

orders = northwind['orders']
orderDetails = northwind['order-details']
products = northwind['products']

pipeline = [{"$match": {"ProductID": 7}},
            {"$group": {"_id": "$ProductID", "Orders": {"$addToSet": "$OrderID"}, "Count": {"$sum": 1}}}]

ordersOfSeven = orderDetails.aggregate(pipeline).next()[u'Orders']
buyersOfSeven = [order[u'CustomerID'] for order in orders.find({"OrderID": {"$in": ordersOfSeven}})]

ordersOfBuyersOfSeven = [orderID[u'OrderID'] for orderID in
                         orders.find({"CustomerID": {"$in": buyersOfSeven}}, {"OrderID": 1, "_id": 0})]

pipeline = [{"$match": {"OrderID": {"$in": ordersOfBuyersOfSeven}}},
            {"$match": {"ProductID": {"$ne": 7}}},
            {"$group": {"_id": "$ProductID", "totalOrders": {"$sum": 1}}}]

productIDsPurchasedFromBuyersOfSeven = [productQuantity for productQuantity in orderDetails.aggregate(pipeline)]
productsPurchasedFromBuyersOfSeven = \
    [product for product in
     products.find({"ProductID": {
         "$in": [productID[u'_id'] for productID in
                 productIDsPurchasedFromBuyersOfSeven]}})]

mostPurchasedProductIdFromBuyersOfSeven = max(*productIDsPurchasedFromBuyersOfSeven, key=lambda x: x[u'totalOrders'])
mostPurchasedProductFromBuyersOfSeven = products.find(
    {"ProductID": mostPurchasedProductIdFromBuyersOfSeven[u'_id']}).next()

print "Most Purchased Product From buyers Of Product Seven:"
print "%s\t%s" % (product[u'ProductID'], product[u'ProductName'])
