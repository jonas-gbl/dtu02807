from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
northwind = client['Northwind']

orders = northwind['orders']
orderDetails = northwind['order-details']
alfki_orders = [orderId[u'OrderID'] for orderId in
                orders.find({"CustomerID": "ALFKI"}, {"OrderID": 1, "_id": 0})]

pipeline = [{"$match": {"OrderID": {"$in": alfki_orders}}},
            {"$group": {"_id": "$OrderID", "Products": {"$push": "$ProductID"}, "Count": {"$sum": 1}}}]

print "The OrderIDs of ALFKI's orders and their products are:"

for order in orderDetails.aggregate(pipeline):
    print "%s\t%s" % (order[u'_id'], order[u'Products'])
