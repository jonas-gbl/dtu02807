from FlajoletMartinEstimator import FlajoletMartinEstimator

flajoletMartinEstimator = FlajoletMartinEstimator()

with open('input/shakespeare.txt') as shakespeare_file:
    for line in shakespeare_file:
        for word in line.split():
            flajoletMartinEstimator.process(word)

cardinality = flajoletMartinEstimator.estimate_cardinality()
print 'Estimated cardinality is %s' % cardinality

