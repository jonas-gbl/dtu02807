SELECT * 
FROM [orders] INNER JOIN [order details]  USING (OrderID) WHERE CustomerId='ALFKI';

SELECT [orders].OrderID, COUNT(*)
FROM [orders] INNER JOIN [order details]  USING (OrderID) WHERE CustomerId='ALFKI'
GROUP BY [orders].OrderID HAVING COUNT(*)>2;

SELECT  [orders].CustomerID FROM [order details] 
INNER JOIN orders ON [order details].OrderID = [orders].OrderID
INNER JOIN [customers] ON [orders].CustomerID = [customers].CustomerID
WHERE   ProductID=7;

SELECT DISTINCT ProductID FROM 
[order details] INNER JOIN [orders] USING (OrderID)
WHERE CustomerID IN (SELECT  [orders].CustomerID FROM [order details] 
INNER JOIN orders ON [order details].OrderID = [orders].OrderID
INNER JOIN [customers] ON [orders].CustomerID = [customers].CustomerID
WHERE   ProductID=7) AND ProductId != 7;

SELECT  ProductID, COUNT(*) FROM 
[order details] INNER JOIN [orders] USING (OrderID)
WHERE CustomerID IN (SELECT  [orders].CustomerID FROM [order details] 
INNER JOIN orders ON [order details].OrderID = [orders].OrderID
INNER JOIN [customers] ON [orders].CustomerID = [customers].CustomerID
WHERE   ProductID=7) AND ProductId != 7 GROUP BY ProductID ORDER BY COUNT(*) DESC LIMIT 1;

SELECT CustomerID, SUM(Quantity) FROM [order details] INNER JOIN [orders] USING (OrderID)
WHERE ProductID IN (SELECT DISTINCT ProductID FROM [order details] INNER JOIN [orders] USING (OrderID)
					WHERE CustomerID='ALFKI')
GROUP BY CustomerID ORDER BY SUM(Quantity) DESC