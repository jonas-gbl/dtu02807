from bitarray import bitarray
from mmh3 import hash


class BloomFilter:
    def __init__(self):
        self._bits = bitarray(1000000)
        self._bits.setall(False)

    def add(self, key):
        hash_v1 = hash(key, 15) % 1000000
        hash_v2 = hash(key, 2) % 1000000
        hash_v3 = hash(key, 86) % 1000000

        self._bits[hash_v1] = True
        self._bits[hash_v2] = True
        self._bits[hash_v3] = True

    def lookup(self, key):
        hash_v1 = hash(key, 15) % 1000000
        hash_v2 = hash(key, 2) % 1000000
        hash_v3 = hash(key, 86) % 1000000

        return self._bits[hash_v1] & self._bits[hash_v2] & self._bits[hash_v3]
