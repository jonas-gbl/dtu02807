from BloomFilter import BloomFilter

bloomFilter = BloomFilter()
dictionary = {}
falsePositives = []

with open('input/dict', 'r') as dict_file:
    for line in dict_file:
        for word in line.split():
            bloomFilter.add(word)
            dictionary[word] = True

with open('input/shakespeare.txt') as shakespeare_file, open('output/falsePositives.txt', 'w+') as falsePositives_file:
    for line in shakespeare_file:
        for word in line.split():
            if bloomFilter.lookup(word) and word not in dictionary:
                falsePositives.append(word)
                falsePositives_file.write(word+'\n')
