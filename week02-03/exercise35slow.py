def slow_sum():
    result = 0.0

    for k in range(1, 10000 + 1):
        result += 1.0 / k ** 2
    return result

if __name__ == "__main__":
    print slow_sum()