from sklearn.ensemble import RandomForestClassifier
from glob import glob
import timeit
import json
import math
import numpy as np
import jsonpath_rw as jpath

json_content = []
for json_filename in glob('./json/*.json'):
    with open(json_filename) as json_file:
        file_content = json.load(json_file)
        filtered_content = filter(lambda u: u'body' in u and len(u[u'body']) and u'topics' in u and len(u[u'topics']),
                                  file_content)
        json_content.extend(filtered_content)

word_idx = {}
global_counter = int(0)

for document in json_content:
    for word in document[u'body'].split():
        token = word.lower()
        # If the word has not been found before
        if token not in word_idx:
            # Assign a new index-number to it
            word_idx[token] = global_counter
            global_counter += 1

start = timeit.default_timer()

word_bags = np.zeros((len(json_content), len(word_idx)), dtype=np.int)

# Iterating through all the words in the request texts
for k in range(len(json_content)):
    for word in json_content[k][u'body'].split():
        token = word.lower()
        # Retrieve the index-number of the current word which was assigned previously
        index = word_idx[token]
        # Increment the counter of specific word on the current request-text
        word_bags[k, index] += 1

# JSON Path expression to retrieve the topic of each JSON document
json_path_expr = jpath.parse('$[*].topics')

# Collected all the requester_received_pizza values as 0/1
topic_is_earn = [int(u'earn' in match.value) for match in json_path_expr.find(json_content)]
topic_is_earn = np.array(topic_is_earn)

n_samples = word_bags.shape[0]
n_training = int(math.floor(0.9 * n_samples))

# Training Data
training_x = word_bags[0:n_training, :]
training_y = topic_is_earn[0:n_training]

# Validation Data
test_x = word_bags[n_training:, :]
test_y = topic_is_earn[n_training:]

# Train classifier
classifier = RandomForestClassifier(n_estimators=50)
classifier.fit(training_x, training_y)

stop = timeit.default_timer()

# Calculate success rate
predicted_y = classifier.predict(test_x)
success_y = predicted_y == test_y
success_y = success_y.astype(int)
success_rate = success_y.sum() / float(len(test_y))

print 'Success rate was: ' + str(success_rate)
print 'Runtime was ' + str(stop - start)
print 'Problem size was ' + str(word_bags.shape)