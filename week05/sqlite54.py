import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT [orders].CustomerID FROM [order details] "
                   "INNER JOIN orders ON [order details].OrderID = [orders].OrderID "
                   "INNER JOIN [customers] ON [orders].CustomerID = [customers].CustomerID "
                   "WHERE ProductID=7")
    rows = cursor.fetchall()

    print "Total customers ordering ProductID=7: %s" % len(rows)
    print ""
    print "Customer ID"
    for row in rows:
        print "%s" % (row["CustomerID"])
