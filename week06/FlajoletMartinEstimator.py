from bitarray import bitarray
from mmh3 import hash


class FlajoletMartinEstimator:
    def __init__(self):
        self._phi = 0.77351
        self._bits = bitarray(32)
        self._bits.setall(False)

    @staticmethod
    def trailing_zeroes(num):
        if num == 0:
            return 32
        p = 0
        while (num >> p) & 1 == 0:
            p += 1
        return p

    def process(self, element):
        key = hash(element)
        if key > 0:
            index = self.trailing_zeroes(key)
            self._bits[index] = True

    def estimate_cardinality(self):
        r = 0
        print self._bits

        while self._bits[r] and r < len(self._bits):
            r += 1
        print r
        return (2 ** r) / self._phi
