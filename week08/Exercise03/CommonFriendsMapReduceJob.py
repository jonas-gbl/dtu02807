from mrjob.job import MRJob
from mrjob.step import MRStep


class CommonFriendsMapReduceJob(MRJob):
    def mapper_get_directional_friendships(self, _, line):
        friends = line.split()
        yield friends[0], friends[1]
        yield friends[1], friends[0]

    def reducer_get_friend_lists(self, person, friends):
        yield person, list(friends)

    def mapper_get_other_friends(self, person, friend_list):
        friend_set = set(friend_list)
        for friend in friend_set:
            other_friends = friend_set.copy()
            other_friends.remove(friend)
            pair_of_friends = sorted([person, friend])
            yield pair_of_friends, list(other_friends)

    def reducer_get_common_friends(self, pair_of_friends, sets_of_other_friends):
        common_friends = set(next(sets_of_other_friends)) & set(next(sets_of_other_friends))
        yield pair_of_friends, list(common_friends)

    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_directional_friendships, reducer=self.reducer_get_friend_lists),
            MRStep(mapper=self.mapper_get_other_friends, reducer=self.reducer_get_common_friends)
        ]


if __name__ == '__main__':
    CommonFriendsMapReduceJob.run()
