# Exercise 1
# Read input file
cat input.log \
| tr '[:punct:]' ' ' | tr '[:upper:]' '[:lower:]' \
| sed -r -e 's#^\s+##' -e '/^\s*$/d' -e 's#\s+$##' -e 's#\s+# #g' \
| tr ' ' '\n' \
| sort | uniq -c \
| sort -r | head -n 10 \
| awk '{ print $2 "\t->\t" $1 }'