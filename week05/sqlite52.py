import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT [orders].OrderID, [order details].ProductID "
                   "FROM [orders] INNER JOIN [order details]  USING (OrderID) "
                   "WHERE CustomerId='ALFKI'"
                   "ORDER BY [orders].OrderID;")
    rows = cursor.fetchall()

    print "OrderID\tProduct ID"
    for row in rows:
        print "%s\t%s" % (row["OrderID"], row["ProductID"])
