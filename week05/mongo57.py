from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017/')
northwind = client['Northwind']

orders = northwind['orders'];
orderDetails = northwind['order-details']
alfki_orders = [orderId[u'OrderID'] for orderId in orders.find({"CustomerID": "ALFKI"}, {"OrderID": 1, "_id": 0})]

pipeline = [{"$match": {"OrderID": {"$in": alfki_orders}}},
            {"$group": {"_id": "$OrderID", "Products": {"$push": "$ProductID"}, "Count": {"$sum": 1}}},
            {"$match": {"Count": {"$gt": 1}}}]

alfki_products = []
for order in orderDetails.aggregate(pipeline):
    alfki_products.extend(order[u'Products'])

pipeline = [{"$match": {"ProductID": {"$in": alfki_products}}},
            {"$group": {"_id": "$OrderID", "Products": {"$push": "$ProductID"},
                        "totalQuantity": {"$sum": "$Quantity"}}}]

ordersWithAlfkiProducts = orderDetails.aggregate(pipeline);
orderWithMaxAlfkiProducts = max(*ordersWithAlfkiProducts, key=lambda x: x[u'totalQuantity'])[u'_id']

customerWithMaxAlfkiProducts = orders.find_one({"OrderID": orderWithMaxAlfkiProducts})
print "The client with the most similar orders in terms of total quantity is %s" % customerWithMaxAlfkiProducts[
    u'CustomerID']
