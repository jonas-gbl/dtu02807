import json
import re

bag_idx = {}
global_counter = int(0)

with open('inputs/pizza-train.json') as json_file:
    pizza_data = json.load(json_file)

# Iterating through all the words in the request texts
# The goal is to assign an index to each distinct word.
# These will be used to index the global word list
for record in pizza_data:
    # Using a RegEx to extract the words
    for word in re.findall('[a-zA-Z\']+', record['request_text']):
        # If the word has not been found before
        if word not in bag_idx:
            # Assign a new index-number to it
            bag_idx[word] = global_counter
            global_counter += 1

bag_of_words = [[0 for x in range(len(bag_idx))] for x in range(len(pizza_data))]

# Iterating through all the words in the request texts
for k in range(len(pizza_data)):
    for word in re.findall('[a-zA-Z\']+', pizza_data[k]['request_text']):
        # Retrieve the index-number of the current word which was assigned previously
        index = bag_idx[word]
        # Increment the counter of specific word on the current request-text
        bag_of_words[k][index] += 1

# Construct the list of unique words found
word_list = [None] * len(bag_idx)
for pair in bag_idx.items():
    word_list[pair[1]] = pair[0]

del bag_idx
del global_counter
