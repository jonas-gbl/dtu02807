import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT CustomerID, SUM(Quantity) AS sum FROM [order details] INNER JOIN [orders] USING (OrderID) "
                   "WHERE ProductID IN "
                   "(SELECT DISTINCT ProductID FROM [order details] INNER JOIN [orders] USING (OrderID) "
                   "WHERE CustomerID='ALFKI')"
                   "GROUP BY CustomerID ORDER BY SUM(Quantity) DESC")
    rows = cursor.fetchall()

    print "Total number of customers buying same products with customer 'ALFKI': %s" % len(rows)
    print ""
    print "CustomerID\tProducts ordered"
    for row in rows:
        print "%s\t%s" % (row["CustomerID"], row["sum"])
