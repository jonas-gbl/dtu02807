for f in *.csv
do
    filename=$(basename "$f")
    extension="${filename##*.}"
    filename="${filename%.*}"
    echo "Processing file $f"
    mongoimport --host=127.0.0.1 -d Northwind -c "$filename" --type csv --file "$f" --headerline
done