import numpy as np
from scipy.interpolate import interp1d
from scipy.optimize import fsolve

A = np.genfromtxt("inputs/ex32.input.txt", delimiter=' ')
x = A[:, 0]
y = A[:, 1]

f_irp = interp1d(x, y, kind='cubic')
x_num = fsolve(f_irp, 0.0)

print x_num
