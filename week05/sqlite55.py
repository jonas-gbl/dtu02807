import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT DISTINCT ProductID FROM "
                   "[order details] INNER JOIN [orders] USING (OrderID) "
                   "WHERE CustomerID IN (SELECT  [orders].CustomerID FROM [order details] "
                   "INNER JOIN orders ON [order details].OrderID = [orders].OrderID "
                   "WHERE   ProductID=7) AND ProductId != 7;")
    rows = cursor.fetchall()

    print "Total number of products: %s" % len(rows)
    print ""
    print "Product ID"
    for row in rows:
        print "%s" % (row["productID"])
