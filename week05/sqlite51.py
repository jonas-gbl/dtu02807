import sqlite3

conn = sqlite3.connect('input/northwind.db')


with conn:
    cursor = conn.cursor()
    conn.row_factory = sqlite3.Row

    cursor.execute('SELECT * FROM Products LIMIT 10')
    rows = cursor.fetchall()

    for row in rows:
        print row
