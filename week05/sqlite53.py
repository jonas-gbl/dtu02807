import sqlite3

conn = sqlite3.connect('input/northwind.db')

with conn:
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    cursor.execute("SELECT [orders].OrderID, COUNT(*) AS count "
                   "FROM [orders] INNER JOIN [order details]  USING (OrderID) WHERE CustomerId='ALFKI' "
                   "GROUP BY [orders].OrderID HAVING COUNT(*)>=2;")
    rows = cursor.fetchall()

    print "OrderID\tCount"
    for row in rows:
        print "%s\t%s" % (row["OrderID"], row["count"])
