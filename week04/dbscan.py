import cPickle as pickle

import numpy as np


class Status:
    """ Simple class used as an Enum """

    def __init__(self):
        pass

    New, Visited, Noise = range(3)


_status = None
""" Global variable to track the status of each point """
_is_member = None
""" Global variable to track membership of each point """
_distance = None
""" Global variable to store distance between points """
_distance_idx = None;
""" Global variable to store indexes of ordered neghbors based on distance """


def dbscan(points, eps=0.3, min_pts=3):
    """
    This function performs the actual DBSCAN on a scipy.sparse.csr_matrix
    as described in the Wikipedia article
    """
    clusters = []
    n_pts = points.shape[0]

    # Pre-computing the distances of the points to be clustered
    _pre_compute_distance(points)

    for k in range(n_pts):
        if _status[k] == Status.Visited:
            continue
        _status[k] = Status.Visited
        neighbors = _region_query(k, eps)
        if len(neighbors) < min_pts:
            _status[k] = Status.Noise
        else:
            cluster = _expand_cluster(k, neighbors, eps, min_pts)
            clusters.append(cluster)

    return clusters


def _expand_cluster(point, neighbors, eps, min_pts):
    global _status
    global _is_member

    cluster = []
    _add_to_cluster(cluster, point)

    while neighbors:
        k = neighbors.pop()
        if _status[k] != Status.Visited:
            _status[k] = Status.Visited
            extended_neighbors = _region_query(k, eps)
            if len(extended_neighbors) >= min_pts:
                neighbors.update(extended_neighbors)
        if not _is_member[k]:
            _add_to_cluster(cluster, k)

    return cluster


def _region_query(center, eps):
    global _distance
    # Optimization 1: Using set because when point A is discovered through point B in _region_Query,
    # it will put B again in the expanded cluster. Point B will be already marked as visited
    # but still if a list was used it would be necessary to iterate through.
    neighbors = set()

    i = 0
    # Optimization 2: Using presorted/precomputed distances, as soon the closest accepted neighbors are
    # found iteration stops
    while _distance[center, _distance_idx[center, i]] <= eps:
        neighbors.add(_distance_idx[center, i])
        i += 1
    return neighbors


def _pre_compute_distance(points):
    global _is_member
    global _status
    global _distance
    global _distance_idx

    n_pts = points.shape[0]

    _distance = np.zeros((n_pts, n_pts))
    _is_member = [False] * n_pts
    _status = [Status.New] * n_pts

    # Since the array has binary format, the row-sum will give the number of non-zero elements
    n = points.sum(axis=1).dot(np.ones((1, n_pts))).A

    # Although the distance array is symmetric and only the upper part has to be computed (the full array can be con-
    # structed using A + A.T) this involved conversions between sparse and dense representations, so sparsity was ex-
    # ploited (through scipy)to compute the full array
    n_intersect = points.dot(points.transpose())
    _distance = 1.0 - (n_intersect / (n + n.T - n_intersect))
    # The neighbors/column-indexes are sorted based on distance, to decrease the running-time of _range_query method
    _distance_idx = np.argsort(_distance, axis=1)


def _add_to_cluster(cluster, k):
    cluster.append(k)
    _is_member[k] = True


if __name__ == "__main__":

    file_descriptor = open('inputs/data_10000points_10000dims.dat', 'r')
    points_to_cluster = pickle.load(file_descriptor)
    _pre_compute_distance(points_to_cluster)
    clusters = dbscan(points_to_cluster, eps=0.15, min_pts=2)
    if len(clusters) > 0:
        print "The following clusters were found:"
        for cluster in clusters:
            print cluster
        noise = [point for point, status in enumerate(_status) if status == Status.Noise]
        print ''
        print 'These are the noise points:'
        print noise
    else:
        print "No clusters found!"
