from sklearn import linear_model
import numpy as np
import jsonpath_rw as jpath
import json
import re
import math

word_idx = {}
global_counter = int(0)

with open('inputs/pizza-train.json') as json_file:
    pizza_data = json.load(json_file)

# Iterating through all the words in the request texts
# The goal is to assign an index to each distinct word.
# These will be used to index the global word list
for record in pizza_data:
    # Using a RegEx to extract the words
    for word in re.findall('[a-zA-Z\']+', record['request_text']):
        # If the word has not been found before
        if word not in word_idx:
            # Assign a new index-number to it
            word_idx[word] = global_counter
            global_counter += 1

word_bags = np.zeros((len(pizza_data), len(word_idx)), dtype=np.int)

# Iterating through all the words in the request texts
for k in range(len(pizza_data)):
    for word in re.findall('[a-zA-Z\']+', pizza_data[k]['request_text']):
        # Retrieve the index-number of the current word which was assigned previously
        index = word_idx[word]
        # Increment the counter of specific word on the current request-text
        word_bags[k, index] += 1

# JSON Path expression to retrieve all the requester_received_pizza fields
json_path_expr = jpath.parse('$[*].requester_received_pizza')

# Collected all the requester_received_pizza values as 0/1
pizza_received = [int(match.value) for match in json_path_expr.find(pizza_data)]
pizza_received = np.array(pizza_received)

n_samples = word_bags.shape[0]
n_training = int(math.floor(0.9 * n_samples))

# Training Data
training_x = word_bags[0:n_training, :]
training_y = pizza_received[0:n_training]

# Validation Data
test_x = word_bags[n_training:, :]
test_y = pizza_received[n_training:]

# Train classifier
classifier = linear_model.LogisticRegression()
classifier.fit(training_x, training_y)

# Calculate success rate
predicted_y = classifier.predict(test_x)
success_y = predicted_y == test_y
success_y = success_y.astype(int)
success_rate = success_y.sum() / float(len(test_y))

print 'Success rate was: ' + str(success_rate)
